package api

import (
	"encoding/json"
	"strings"
	"time"

	"github.com/gofiber/fiber/v2"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func Parse(data []byte) (result Object, err error) {
	err = json.Unmarshal(data, &result)
	return
}

func Objectify(fromAddress interface{}) (result Object, err error) {
	result, err = ParseObject(fromAddress)
	return
}

func New(c *fiber.Ctx) (a *API, err error) {
	a = &API{}
	a.Ctx = c
	contentType := string(a.Request().Header.ContentType())
	if strings.EqualFold(contentType, "application/json") {
		a.Payload, err = Parse(c.Body())
	}
	return
}

func Convert(fromAddress interface{}, toAddress interface{}) (err error) {
	data, err := json.Marshal(fromAddress)
	if err != nil {
		return
	}
	err = json.Unmarshal(data, toAddress)
	return
}

func ParseTime(i interface{}) (result time.Time, err error) {
	switch data := i.(type) {
	case string:
		result, err = time.Parse(time.RFC3339, data)
	case primitive.DateTime:
		result = data.Time()
	default:
		err = ErrParseTime
	}
	return
}

func ParseOID(i interface{}) (result primitive.ObjectID, err error) {
	switch data := i.(type) {
	case string:
		result, err = primitive.ObjectIDFromHex(data)
	case primitive.ObjectID:
		result = data
	default:
		err = ErrParseOID
	}
	return
}

func ParseArray(i interface{}) (result []interface{}, err error) {
	switch data := i.(type) {
	case []interface{}:
		result = data
	case primitive.A:
		result = data
	default:
		err = ErrParseArray
	}
	return
}

func ParseObject(i interface{}) (result map[string]interface{}, err error) {
	switch data := i.(type) {
	case map[string]interface{}:
		result = data
	case *map[string]interface{}:
		result = *data
	case primitive.M:
		result = data
	case *primitive.M:
		result = *data
	case fiber.Map:
		result = data
	case *fiber.Map:
		result = *data
	case Object:
		result = data
	case *Object:
		result = *data
	default:
		err = Convert(data, &result)
	}
	return
}

func ParseInt(i interface{}) (result int, err error) {
	switch data := i.(type) {
	case int:
		result = data
	case int32:
		result = int(data)
	case int64:
		result = int(data)
	case float32:
		result = int(data)
	case float64:
		result = int(data)
	default:
		err = ErrParseInt
	}
	return
}

func ParseInt32(i interface{}) (result int32, err error) {
	switch data := i.(type) {
	case int:
		result = int32(data)
	case int32:
		result = data
	case int64:
		result = int32(data)
	case float32:
		result = int32(data)
	case float64:
		result = int32(data)
	default:
		err = ErrParseInt32
	}
	return
}

func ParseInt64(i interface{}) (result int64, err error) {
	switch data := i.(type) {
	case int:
		result = int64(data)
	case int32:
		result = int64(data)
	case int64:
		result = data
	case float32:
		result = int64(data)
	case float64:
		result = int64(data)
	default:
		err = ErrParseInt64
	}
	return
}

func ParseFloat(i interface{}) (result float64, err error) {
	switch data := i.(type) {
	case int:
		result = float64(data)
	case int32:
		result = float64(data)
	case int64:
		result = float64(data)
	case float32:
		result = float64(data)
	case float64:
		result = data
	default:
		err = ErrParseFloat
	}
	return
}

func ParseFloat32(i interface{}) (result float32, err error) {
	switch data := i.(type) {
	case int:
		result = float32(data)
	case int32:
		result = float32(data)
	case int64:
		result = float32(data)
	case float32:
		result = data
	case float64:
		result = float32(data)
	default:
		err = ErrParseFloat32
	}
	return
}

func ParseFloat64(i interface{}) (result float64, err error) {
	switch data := i.(type) {
	case int:
		result = float64(data)
	case int32:
		result = float64(data)
	case int64:
		result = float64(data)
	case float32:
		result = float64(data)
	case float64:
		result = data
	default:
		err = ErrParseFloat64
	}
	return
}
