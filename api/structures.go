package api

import "github.com/gofiber/fiber/v2"

type Object map[string]interface{}

type API struct {
	*fiber.Ctx
	Payload Object
}

type Response struct {
	Status  string      `json:"status" bson:"status"`
	Message string      `json:"message,omitempty" bson:"message,omitempty"`
	Data    interface{} `json:"data,omitempty" bson:"data,omitempty"`
}
