package api

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"
	"sync"
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

func (obj Object) Unwind() {
	mu := &sync.Mutex{}
	wg := &sync.WaitGroup{}
	childResult := Object{}
	for key, value := range obj {
		wg.Add(1)
		keyCpy := key
		valueCpy := value
		go func() {
			defer wg.Done()
			childObj, _ := Objectify(valueCpy)
			childObj.Unwind()
			for childKey, childValue := range childObj {
				mu.Lock()
				childResult[keyCpy+"."+childKey] = childValue
				mu.Unlock()
			}
		}()
	}
	wg.Wait()
	for key, value := range childResult {
		obj[key] = value
	}
}

func (obj Object) Scan(keys map[string]string) (err error) {
	message := ""
	sep := ""
	for key, value := range keys {
		switch strings.ToLower(value) {
		case "bool":
			_, err = obj.GetBool(key)
		case "int":
			_, err = obj.GetInt(key)
		case "int32":
			_, err = obj.GetInt32(key)
		case "int64":
			_, err = obj.GetInt64(key)
		case "float":
			_, err = obj.GetFloat(key)
		case "float32":
			_, err = obj.GetFloat32(key)
		case "float64":
			_, err = obj.GetFloat64(key)
		case "string":
			_, err = obj.GetString(key)
		case "oid":
			_, err = obj.GetOID(key)
		case "array":
			_, err = obj.GetArray(key)
		case "time":
			_, err = obj.GetTime(key)
		case "object":
			_, err = obj.GetObject(key)
		default:
			_, err = obj.Get(key)
		}
		if err != nil {
			message = fmt.Sprintf("%[1]s%[2]s%[3]v;", message, sep, err)
			sep = " "
		}
	}
	if message != "" {
		err = errors.New(message)
	}
	return
}

func (obj Object) Get(path string) (result interface{}, err error) {
	result, ok := obj[path]
	if ok {
		return
	}
	keys := strings.Split(path, ".")
	lengthOfKeys := len(keys)
	for i := 0; i < lengthOfKeys; i++ {
		var ok bool
		result, ok = obj[keys[i]]
		if !ok {
			err = fmt.Errorf("%[1]s: %[2]v", keys[i], ErrParseKey)
			return
		}
		if i < lengthOfKeys-1 {
			var childObj Object
			childObj, err = Objectify(result)
			if err != nil {
				result = nil
				return
			}
			obj = childObj
		}
	}
	return
}

func (obj Object) GetObject(key string) (result Object, err error) {
	value, err := obj.Get(key)
	if err != nil {
		return
	}
	result, err = ParseObject(value)
	if err != nil {
		err = fmt.Errorf("%[1]s: %[2]v", key, err)
	}
	return
}

func (obj Object) GetString(key string) (result string, err error) {
	value, err := obj.Get(key)
	if err != nil {
		return
	}
	result, ok := value.(string)
	if !ok {
		err = fmt.Errorf("%[1]s: %[2]v", key, ErrParseString)
	}
	return
}

func (obj Object) GetBool(key string) (result bool, err error) {
	value, err := obj.Get(key)
	if err != nil {
		return
	}
	result, ok := value.(bool)
	if !ok {
		err = fmt.Errorf("%[1]s: %[2]v", key, ErrParseBool)
	}
	return
}

func (obj Object) GetInt(key string) (result int, err error) {
	value, err := obj.Get(key)
	if err != nil {
		return
	}
	result, err = ParseInt(value)
	if err != nil {
		err = fmt.Errorf("%[1]s: %[2]v", key, err)
	}
	return
}

func (obj Object) GetInt32(key string) (result int32, err error) {
	value, err := obj.Get(key)
	if err != nil {
		return
	}
	result, err = ParseInt32(value)
	if err != nil {
		err = fmt.Errorf("%[1]s: %[2]v", key, err)
	}
	return
}

func (obj Object) GetInt64(key string) (result int64, err error) {
	value, err := obj.Get(key)
	if err != nil {
		return
	}
	result, err = ParseInt64(value)
	if err != nil {
		err = fmt.Errorf("%[1]s: %[2]v", key, err)
	}
	return
}

func (obj Object) GetFloat(key string) (result float64, err error) {
	value, err := obj.Get(key)
	if err != nil {
		return
	}
	result, err = ParseFloat(value)
	if err != nil {
		err = fmt.Errorf("%[1]s: %[2]v", key, err)
	}
	return
}

func (obj Object) GetFloat32(key string) (result float32, err error) {
	value, err := obj.Get(key)
	if err != nil {
		return
	}
	result, err = ParseFloat32(value)
	if err != nil {
		err = fmt.Errorf("%[1]s: %[2]v", key, err)
	}
	return
}

func (obj Object) GetFloat64(key string) (result float64, err error) {
	value, err := obj.Get(key)
	if err != nil {
		return
	}
	result, err = ParseFloat64(value)
	if err != nil {
		err = fmt.Errorf("%[1]s: %[2]v", key, err)
	}
	return
}

func (obj Object) GetOID(key string) (result primitive.ObjectID, err error) {
	value, err := obj.Get(key)
	if err != nil {
		return
	}
	result, err = ParseOID(value)
	if err != nil {
		err = fmt.Errorf("%[1]s: %[2]v", key, err)
	}
	return
}

func (obj Object) GetTime(key string) (result time.Time, err error) {
	value, err := obj.Get(key)
	if err != nil {
		return
	}
	result, err = ParseTime(value)
	if err != nil {
		err = fmt.Errorf("%[1]s: %[2]v", key, err)
	}
	return
}

func (obj Object) GetArray(key string) (result []interface{}, err error) {
	value, err := obj.Get(key)
	if err != nil {
		return
	}
	result, err = ParseArray(value)
	if err != nil {
		err = fmt.Errorf("%[1]s: %[2]v", key, err)
	}
	return
}

func (obj Object) GetObjectArray(key string) (result []Object, err error) {
	array, err := obj.GetArray(key)
	if err != nil {
		return
	}
	var object Object
	for i, element := range array {
		object, err = ParseObject(element)
		if err != nil {
			err = fmt.Errorf("%[1]s[%[2]d]: %[3]v", key, i, err)
			result = nil
			return
		}
		result = append(result, object)
	}
	return
}

func (obj Object) GetStringArray(key string) (result []string, err error) {
	array, err := obj.GetArray(key)
	if err != nil {
		return
	}
	for i, element := range array {
		value, ok := element.(string)
		if !ok {
			err = fmt.Errorf("%[1]s[%[2]d]: %[3]v", key, i, ErrParseString)
			result = nil
			return
		}
		result = append(result, value)
	}
	return
}

func (obj Object) GetOIDArray(key string) (result []primitive.ObjectID, err error) {
	array, err := obj.GetArray(key)
	if err != nil {
		return
	}
	var objectID primitive.ObjectID
	for i, element := range array {
		objectID, err = ParseOID(element)
		if err != nil {
			err = fmt.Errorf("%[1]s[%[2]d]: %[3]v", key, i, err)
			result = nil
			return
		}
		result = append(result, objectID)
	}
	return
}

func (obj Object) GetIntArray(key string) (result []int, err error) {
	array, err := obj.GetArray(key)
	if err != nil {
		return
	}
	var number int
	for i, element := range array {
		number, err = ParseInt(element)
		if err != nil {
			err = fmt.Errorf("%[1]s[%[2]d]: %[3]v", key, i, err)
			result = nil
			return
		}
		result = append(result, number)
	}
	return
}

func (obj Object) GetFloatArray(key string) (result []float64, err error) {
	array, err := obj.GetArray(key)
	if err != nil {
		return
	}
	var number float64
	for i, element := range array {
		number, err = ParseFloat(element)
		if err != nil {
			err = fmt.Errorf("%[1]s[%[2]d]: %[3]v", key, i, err)
			result = nil
			return
		}
		result = append(result, number)
	}
	return
}

func (obj Object) GetTimeArray(key string) (result []time.Time, err error) {
	array, err := obj.GetArray(key)
	if err != nil {
		return
	}
	var date time.Time
	for i, element := range array {
		date, err = ParseTime(element)
		if err != nil {
			err = fmt.Errorf("%[1]s[%[2]d]: %[3]v", key, i, err)
			result = nil
			return
		}
		result = append(result, date)
	}
	return
}

func (obj Object) GetOIDs(ch chan<- primitive.ObjectID, keys ...string) {
	defer close(ch)
	for _, key := range keys {
		value, _ := obj.GetOID(key)
		ch <- value
	}
}

func (obj Object) GetStrings(ch chan<- string, keys ...string) {
	defer close(ch)
	for _, key := range keys {
		value, _ := obj.GetString(key)
		ch <- value
	}
}

func (obj Object) GetTimes(ch chan<- time.Time, keys ...string) {
	defer close(ch)
	for _, key := range keys {
		value, _ := obj.GetTime(key)
		ch <- value
	}
}

func (obj Object) Decode(toAddress interface{}) (err error) {
	bytes, err := json.Marshal(obj)
	if err != nil {
		return
	}
	err = json.Unmarshal(bytes, &toAddress)
	return
}

func (a *API) Success(data ...interface{}) error {
	response := &Response{
		Status: "success",
	}
	if data != nil && data[0] != nil {
		response.Data = data[0]
	}
	return a.Status(200).JSON(response)
}

func (a *API) Failure(message ...interface{}) error {
	response := &Response{
		Status: "failure",
	}
	if message != nil && message[0] != nil {
		response.Message = fmt.Sprintf("%[1]v", message[0])
	}
	return a.Status(200).JSON(response)
}
