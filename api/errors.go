package api

import (
	"errors"
)

var (
	/*
		Errors generated from this package.
	*/
	ErrParseKey     = errors.New("key not found")
	ErrParseArray   = errors.New("invalid array")
	ErrParseString  = errors.New("invalid string")
	ErrParseBool    = errors.New("invalid boolean")
	ErrParseInt     = errors.New("invalid int")
	ErrParseInt32   = errors.New("invalid int32")
	ErrParseInt64   = errors.New("invalid int64")
	ErrParseFloat   = errors.New("invalid float")
	ErrParseFloat32 = errors.New("invalid float32")
	ErrParseFloat64 = errors.New("invalid float64")
	ErrParseOID     = errors.New("invalid objectID")
	ErrParseTime    = errors.New("invalid time")
	ErrType         = errors.New("unsupported type")
)
