module bitbucket.org/rvram/golang-packages

go 1.15

require (
	github.com/gofiber/fiber/v2 v2.0.4
	go.mongodb.org/mongo-driver v1.4.1
)
